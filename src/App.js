import React, { useState } from 'react';
import './App.css';
import { makeStyles } from '@material-ui/styles'
import { CssBaseline, Grid, Typography, Button, ListItem, List, Hidden, IconButton, Drawer } from '@material-ui/core';
import logoName from './assets/brand.png';
import logoMobile from './assets/logoMobile.png';
import bg1 from './assets/bg1.png';
import bg2 from './assets/bg2.png';
import content1 from './assets/content1.png';
import content2 from './assets/content2.png';
import content3 from './assets/content3.png';
import logoOnly from './assets/logoOnly.png'
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import { EntypoFacebookWithCircle, EntypoTwitterWithCircle, EntypoLinkedinWithCircle, EntypoMailWithCircle } from 'react-entypo-icons';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';


const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh'
  },
  navContainerWeb: {
    borderBottom: '5px solid #001254'
  },
  navContainerMobile: {
    backgroundColor: '#ebecf0',
  },
  menuIcon: {
    fill: '#1a3989'
  },
  closeIcon: {
    fill: '#d81e05'
  },
  siteLinks: {
    borderBottom: '1px solid #bfbfbf',
    marginTop: 16,
    marginLeft: 36,
    marginRight: 36,
    marginBottom: 12,
    color: '#001254',
    textAlign: 'center'
  },
  links: {
    backgroundColor: '#ebebeb',
    marginLeft: 36,
    marginRight: 36,
    marginBottom: 12,
    padding: 12,
    color: '#707070',
    textAlign: 'center'
  },
  linkPrivacyContaier: {
    marginLeft: 30,
    marginRight: 30,
  },
  linksBottomPrivary: {
    backgroundColor: '#eaecee',
    padding: 12,
    color: '#707070',
    textAlign: 'center'
  },
  landingContainer: {
    height: '28em',
    color: '#FFF',
  },
  landingContainerMobile: {
    height: '35em',
    color: '#FFF',
    backgroundColor: '#1b9cf8',
    paddingLeft: 16
  },
  insightContainer: {
    backgroundColor: '#ebebeb',
    paddingTop: 24,
    paddingBottom: 24,
    height: '28em',
  },
  eventContainer: {
    backgroundColor: '#ebebeb',
    paddingTop: 24,
    paddingBottom: 24,
  },
  insightContainerMobile: {
    height: '40em',
    color: '#FFF',
    backgroundColor: '#001a7a',
    paddingLeft: 16,
  },
  eventContainerMobile: {
    height: '45em',
    color: '#021573',
    backgroundColor: '#ebebeb',
    paddingLeft: 16,
  },
  cardContainerBlue: {
    backgroundColor: '#fff',
    padding: 18,
    borderBottom: '8px solid #1450d2'
  },
  cardTextBlue: {
    color: '#1450d2',
    fontWeight: 'bold',
  },
  cardContainerBlue2: {
    backgroundColor: '#fff',
    padding: 18,
    borderBottom: '8px solid #009bfa'
  },
  cardTextBlue2: {
    color: '#009bfa',
    fontWeight: 'bold',
  },
  cardContainerGreen: {
    backgroundColor: '#fff',
    padding: 18,
    borderBottom: '8px solid #01a49c'
  },
  cardTextGreen: {
    color: '#01a49c',
    fontWeight: 'bold',
  },
  contactUsContainer: {
    backgroundColor: '#1450d2',
    paddingTop: 24,
    paddingBottom: 24,
    boxShadow: 'inset 0 0 10px  #000000',
    backgroundSize: 'cover',
    height: '28em',
  },
  contactUsButton: {
    padding: 12,
    color: '#f7f7f7',
    border: '1px solid #f7f7f7',
    marginTop: 36
  },
  cardContainerBlueEvent: {
    backgroundColor: '#1450d2',
    padding: 18,
    marginTop: 36,
    height: 300,
  },
  cardTextBlueEvent: {
    color: '#FFF',
    fontWeight: 'bold',
  },
  cardTextDate: {
    color: '#FFF',
    fontSize: 20
  },
  cardTextMonth: {
    color: '#FFF',
  },
  dateContainer: {
    backgroundColor: '#001254',
    height: 70,
    width: 70,
    marginTop: -40,
    marginBottom: 16
  },
  eventButton: {
    padding: 12,
    color: '#f7f7f7',
    border: '1px solid #f7f7f7',
    maxWidth: 150,
    textAlign: 'center',
  },
  footerContainer: {
    backgroundColor: '#707070',
    color: '#fff',
    height: 70,
  },
  socialIcon: {
    fill: '#c6c6c6'
  }

}))

function App() {
  const classes = useStyles();

  const [navOn, setNavOn] = useState(false);
  const [activeStepInsights, setActiveStepInsights] = React.useState(0);
  const [activeStepEvents, setActiveStepEvents] = React.useState(0);


  const handleStepChangeInsights = (step) => {
    setActiveStepInsights(step);
  };

  const handleStepChangeEvents = (step) => {
    setActiveStepEvents(step);
  };

  const ListNav = () => {
    return (
      <Grid item>
        <List>
          <Grid container direction="row" spacing={2}>
            <Grid item>
              <ListItem>
                <Typography>Home</Typography>
              </ListItem>
            </Grid>
            <Grid item>
              <ListItem>
                <Typography>About Us</Typography>
              </ListItem>
            </Grid>
            <Grid item>
              <ListItem>
                <Typography>Insight</Typography>
              </ListItem>
            </Grid>
            <Grid item>
              <ListItem>
                <Typography>Events</Typography>
              </ListItem>
            </Grid>
            <Grid item>
              <ListItem>
                <Typography>Contact Us</Typography>
              </ListItem>
            </Grid>
          </Grid>
        </List>
      </Grid>
    )
  }

  const Card = () => {
    return (
      <>
        <Grid item xs={7}>
          <Grid container direction="row" justify="space-between">
            <Grid item className={classes.cardContainerBlue}>
              <img alt="content1" src={content1} width="200" />
              <Typography variant="h6" className={classes.cardTextBlue}>Global Factor</Typography>
              <Typography style={{ marginBottom: -8 }} variant="h6" className={classes.cardTextBlue}>Investing Study</Typography>
            </Grid>
            <Grid item className={classes.cardContainerGreen}>
              <img alt="content2" src={content2} width="200" />
              <Typography variant="h6" className={classes.cardTextGreen}>2019</Typography>
              <Typography style={{ marginBottom: -8 }} variant="h6" className={classes.cardTextGreen}>Outlook</Typography>
            </Grid>
            <Grid item className={classes.cardContainerBlue2}>
              <img alt="content3" src={content3} width="200" />
              <Typography variant="h6" className={classes.cardTextBlue2}>Capital Market</Typography>
              <Typography style={{ marginBottom: -8 }} variant="h6" className={classes.cardTextBlue2}>Assumptions</Typography>
            </Grid>
          </Grid>
        </Grid>
      </>
    )
  }

  const CardEvent = () => {
    return (
      <>
        <Grid item xs={7}>
          <Grid container direction="row" justify="space-between" >

            <Grid item md={3}>
              <Grid container direction="column" className={classes.cardContainerBlueEvent} justify="space-between">
                <Grid container direction="column" className={classes.dateContainer} justify="center" alignItems="center">
                  <Typography variant="caption" className={classes.cardTextMonth}>JAN</Typography>
                  <Typography className={classes.cardTextDate} >28</Typography>
                </Grid>
                <Grid item style={{ flexGrow: 1 }}>
                  <Typography variant="h6" className={classes.cardTextBlueEvent}>Insight Exchange Network</Typography>
                  <Typography style={{ color: '#fff' }} variant="body1">
                    Join us for this conference showcasing innovation
                </Typography>
                </Grid>
                <Grid item className={classes.eventButton}>
                  <Typography>Get More Insight</Typography>
                </Grid>
              </Grid>
              <Grid container justify="flex-end" >
                <Typography variant="h6" style={{ fontWeight: 'bold', marginTop: 12 }}>Chicago, IL</Typography>
              </Grid>
            </Grid>

            <Grid item md={3}>
              <Grid container direction="column" className={classes.cardContainerBlueEvent} justify="space-between">
                <Grid container direction="column" className={classes.dateContainer} justify="center" alignItems="center">
                  <Typography variant="caption" className={classes.cardTextMonth}>MAY</Typography>
                  <Typography className={classes.cardTextDate} >6</Typography>
                </Grid>
                <Grid item style={{ flexGrow: 1 }}>
                  <Typography variant="h6" className={classes.cardTextBlueEvent}>Citywide Buyer’s Retreat</Typography>
                  <Typography style={{ color: '#fff' }} variant="body1">
                    Find out how banks are responding to the changing future of interest...
                </Typography>
                </Grid>
                <Grid item className={classes.eventButton}>
                  <Typography>Get More Insight</Typography>
                </Grid>
              </Grid>
              <Grid container justify="flex-end" >
                <Typography variant="h6" style={{ fontWeight: 'bold', marginTop: 12 }}>London, England</Typography>
              </Grid>
            </Grid>


            <Grid item md={3}>
              <Grid container direction="column" className={classes.cardContainerBlueEvent} justify="space-between">
                <Grid container direction="column" className={classes.dateContainer} justify="center" alignItems="center">
                  <Typography variant="caption" className={classes.cardTextMonth}>MAY</Typography>
                  <Typography className={classes.cardTextDate} >6</Typography>
                </Grid>
                <Grid item style={{ flexGrow: 1 }}>
                  <Typography variant="h6" className={classes.cardTextBlueEvent}>Research Exchange</Typography>
                  <Typography style={{ color: '#fff' }} variant="body1">
                    Find the best online resources to help with your investments...
                </Typography>
                </Grid>
                <Grid item className={classes.eventButton}>
                  <Typography>Get More Insight</Typography>
                </Grid>
              </Grid>
              <Grid container justify="flex-end" >
                <Typography variant="h6" style={{ fontWeight: 'bold', marginTop: 12 }}>London, England</Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </>
    )
  }

  return (
    <>
      <CssBaseline />
      <Grid container direction="column">
        <Hidden smDown>
          <Grid item xs={12} >
            <Grid container justify="center" alignItems="flex-end" className={classes.navContainerWeb}>
              <Grid item xs={2}>
                <Button>
                  <img src={logoName} alt="acme-corporation" width="180" />
                </Button>
              </Grid>
              <ListNav />
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid 
              container justify="center"
              alignItems="center"
              className={classes.landingContainer}
              style={{
                backgroundImage: `url(${bg1})`,
                backgroundSize: 'cover',
              }}
            >
              <Grid item xs={7}>
                <Typography variant="h4" style={{ fontWeight: 'bold' }}>ACME Wealth</Typography>
                <Typography variant="h4" gutterBottom style={{ fontWeight: 'bold' }}>Management Platforms</Typography>
                <Typography variant="h5">Investment excellence.</Typography>
                <Typography variant="h5">Diversity of thought..</Typography>
                <Typography variant="h5">Organizational strength.</Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12} >
            <Grid 
              container justify="center"
              className={classes.insightContainer}
            >
              <Grid item xs={7}>
                <Typography variant="h6" style={{ fontWeight: 'bold' }}>ACME Insights</Typography>
                <Typography variant="body1" gutterBottom>How are factors being used around the world?</Typography>
              </Grid>
              <Card />
            </Grid>
          </Grid>

          <Grid item xs={12}
          >
            <Grid 
              className={classes.contactUsContainer}
              container justify="center"
              alignItems="center"
              style={{
                backgroundImage: `url(${bg2})`,
                backgroundSize: 'cover',
              }}

            >
              <Grid item xs={7}>
                <Typography variant="h5" gutterBottom style={{ fontWeight: 'bold', textAlign: 'center', color: '#f7f7f7' }}>Our Commitment to Professionals</Typography>
                <Typography variant="body1" gutterBottom style={{ textAlign: 'center', color: '#f7f7f7' }}>
                  We help our partners deliver industry leading results with a commitment to excellence, thought-provoking insights and experienced distribution. We are laser focused on our shared goal – helping clients achieve their objectives.
                </Typography>
                <Grid container justify="center">
                  <Grid item className={classes.contactUsButton}>
                    <Typography>Contact Us</Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12} >
            <Grid container
              justify="center"
              className={classes.eventContainer}
            >
              <Grid item xs={7}>
                <Typography variant="h6" style={{ fontWeight: 'bold' }}>Upcoming Events</Typography>
                <Typography variant="body1" gutterBottom>This needs a great tagline, but I’ll fill it in later</Typography>
              </Grid>
              <CardEvent />
            </Grid>
          </Grid>

          <Grid item xs={12} >
            <Grid container
              justify="center"
              alignItems="center"
              className={classes.footerContainer}
            >
              <Grid item xs={7}>
                <Grid container justify="space-between">
                  <Grid item>
                    <Typography variant="body1">Call us at 111-222-3333 for more information</Typography>
                  </Grid>
                  <Grid item>
                    <Typography variant="body1">Social Share <EntypoFacebookWithCircle className={classes.socialIcon} /> <EntypoTwitterWithCircle className={classes.socialIcon} /> <EntypoLinkedinWithCircle className={classes.socialIcon} /> <EntypoMailWithCircle className={classes.socialIcon} /> </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>


        </Hidden>

        {/************************************* MOBILE *************************************************/}
        <Hidden mdUp>
          <Grid item xs={12} className={classes.navContainerMobile}>
            <Grid container justify="center" alignItems="center">
              <Grid item xs={11} container justify="center">
                <Button>
                  <img src={logoMobile} alt="acme-corporation" width="120" />
                </Button>
              </Grid>
              <Grid item xs={1} container justify="flex-end">
                <IconButton onClick={() => setNavOn(true)}>
                  <MenuIcon className={classes.menuIcon} />
                </IconButton>
              </Grid>
            </Grid>
            <Drawer anchor='top' open={navOn} onClose={() => setNavOn(false)}>
              <Grid container justify="center" alignItems="center" className={classes.navContainerMobile}>
                <Grid item xs={11} container justify="center">
                  <Button>
                    <img src={logoMobile} alt="acme-corporation" width="120" />
                  </Button>
                </Grid>
                <Grid item xs={1} container justify="flex-end">
                  <IconButton onClick={() => setNavOn(false)}>
                    <CloseIcon className={classes.closeIcon} />
                  </IconButton>
                </Grid>
                <Grid item xs={12}>
                  <Grid container style={{ backgroundColor: '#fff' }}>
                    <Grid item xs={12} className={classes.siteLinks}>
                      <Typography>Site Links</Typography>
                    </Grid>
                    <Grid item xs={12} className={classes.links}>
                      <Typography>Home</Typography>
                    </Grid>
                    <Grid item xs={12} className={classes.links}>
                      <Typography>About Us</Typography>
                    </Grid>
                    <Grid item xs={12} className={classes.links}>
                      <Typography>Insight</Typography>
                    </Grid>
                    <Grid item xs={12} className={classes.links}>
                      <Typography>Events</Typography>
                    </Grid>
                    <Grid item xs={12} className={classes.links}>
                      <Typography>Contact Us</Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Drawer>
          </Grid>

          <Grid item xs={12}>
            <Grid
              container
              direction="column"
              className={classes.landingContainerMobile}
            >
              <Grid item style={{ marginTop: 24, marginBottom: 56 }}>
                <Typography variant="body1">Research Professional Platform</Typography>
              </Grid>
              <Grid item style={{ marginTop: 24, marginBottom: 100 }}>
                <Typography variant="body1">ACME Wealth</Typography>
                <Typography variant="h4" style={{ fontWeight: 'bold' }}>Management</Typography>
                <Typography variant="h4" style={{ fontWeight: 'bold' }}>Platforms</Typography>
                <Typography variant="caption" style={{ fontWeight: [200] }}>&#9679; &#9679; &#9679; &#9679; &#9679; &#9679; &#9679; &#9679; &#9679; &#9679; &#9679; &#9679; &#9679; &#9679; &#9679; &#9679; &#9679;</Typography>
                <Typography variant="body1">Investment excellence.</Typography>
                <Typography variant="body1">Diversity of thought.</Typography>
                <Typography variant="body1">Organizational strength.</Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item>
            <Grid
              container
              direction="column"
              className={classes.insightContainerMobile}
            >
              <Grid item style={{ marginTop: 24, marginBottom: 56 }}>
                <Typography variant="h5" >ACME <b>Insights</b></Typography>
                <Typography variant="body1">How are factors being used around the world?</Typography>
              </Grid>
              <Grid item>
                <Grid container direction="row" style={{
                  overflow: 'hidden',
                  maxWidth: 380,
                }}>
                  <AutoPlaySwipeableViews
                    index={activeStepInsights}
                    onChangeIndex={handleStepChangeInsights}
                    enableMouseEvents
                  >
                    {Math.abs(activeStepInsights - 0) <= 2 ? (
                      <Grid item style={{ maxWidth: 385 }}>
                        <Grid container direction="column" className={classes.cardContainerBlue}>

                          <img alt="" src={content1} width="345" />
                          <Typography variant="h6" className={classes.cardTextBlue}>Global Factor</Typography>
                          <Typography style={{ marginBottom: -8 }} variant="h6" className={classes.cardTextBlue}>Investing Study</Typography>
                        </Grid>
                      </Grid>
                    ) : null}
                    {Math.abs(activeStepInsights - 1) <= 2 ? (
                      <Grid item style={{ maxWidth: 385 }}>
                        <Grid container direction="column" className={classes.cardContainerGreen}>
                          <img alt="" src={content2} width="345" />
                          <Typography variant="h6" className={classes.cardTextGreen}>2019</Typography>
                          <Typography style={{ marginBottom: -8 }} variant="h6" className={classes.cardTextGreen}>Outlook</Typography>
                        </Grid>
                      </Grid>
                    ) : null}
                    {Math.abs(activeStepInsights - 2) <= 2 ? (
                      <Grid item style={{ maxWidth: 385 }}>
                        <Grid container direction="column" className={classes.cardContainerBlue2}>

                          <img alt="" src={content3} width="345" />
                          <Typography variant="h6" className={classes.cardTextBlue2}>Capital Market</Typography>
                          <Typography style={{ marginBottom: -8 }} variant="h6" className={classes.cardTextBlue2}>Assumptions</Typography>
                        </Grid>
                      </Grid>
                    ) : null}
                  </AutoPlaySwipeableViews>
                </Grid>
              </Grid>
            </Grid>
          </Grid>

          <Grid item >
            <Grid 
              className={classes.contactUsContainer}
              container justify="center"
              alignItems="center"
              style={{
                backgroundImage: `url(${bg2})`,
                backgroundSize: 'cover',
                paddingLeft: 16,
                paddingRight: 16
              }}
            >
              <Grid item >
                <Typography variant="h5" gutterBottom style={{ fontWeight: 'bold', textAlign: 'center', color: '#f7f7f7' }}>Our Commitment to Professionals</Typography>
                <Typography variant="body1" gutterBottom style={{ textAlign: 'center', color: '#f7f7f7' }}>
                  We help our partners deliver industry leading results with a commitment to excellence, thought-provoking insights and experienced distribution. We are laser focused on our shared goal – helping clients achieve their objectives.
                </Typography>
                <Grid container justify="center">
                  <Grid item className={classes.contactUsButton}>
                    <Typography>Contact Us</Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>

          <Grid item>
            <Grid
              container
              direction="column"
              className={classes.eventContainerMobile}
            >
              <Grid item style={{ marginTop: 24, marginBottom: 56 }}>
                <Typography variant="h5" >ACME <b>Insights</b></Typography>
                <Typography variant="body1" style={{ color: '#6e6e6e' }}>How are factors being used around the world?</Typography>
              </Grid>
              <Grid item>
                <Grid container direction="row" style={{
                  overflow: 'hidden',
                  maxWidth: 380,
                  marginBottom: 36
                }}>
                  <AutoPlaySwipeableViews
                    index={activeStepEvents}
                    onChangeIndex={handleStepChangeEvents}
                    enableMouseEvents
                  >
                    {Math.abs(activeStepEvents - 0) <= 2 ? (
                      <Grid item style={{ maxWidth: 385 }}>
                        <Grid container direction="column" className={classes.cardContainerBlueEvent} justify="space-between">
                          <Grid container direction="column" className={classes.dateContainer} justify="center" alignItems="center">
                            <Typography variant="caption" className={classes.cardTextMonth}>JAN</Typography>
                            <Typography className={classes.cardTextDate} >28</Typography>
                          </Grid>
                          <Grid item container>
                            <Typography variant="h6" className={classes.cardTextBlueEvent}>Insight Exchange Network</Typography>
                            <Typography style={{ color: '#fff' }} variant="body1">
                              Join us for this conference showcasing innovation
                            </Typography>
                          </Grid>
                          <Grid item className={classes.eventButton}>
                            <Typography>Get More Insight</Typography>
                          </Grid>
                        </Grid>
                        <Grid container justify="flex-end" >
                          <Typography variant="h6" style={{ fontWeight: 'bold', marginTop: 12 }}>Chicago, IL</Typography>
                        </Grid>
                      </Grid>

                    ) : null}

                    {Math.abs(activeStepEvents - 0) <= 2 ? (
                      <Grid item style={{ maxWidth: 385 }}>
                        <Grid container direction="column" className={classes.cardContainerBlueEvent} justify="space-between">
                          <Grid container direction="column" className={classes.dateContainer} justify="center" alignItems="center">
                            <Typography variant="caption" className={classes.cardTextMonth}>FEB</Typography>
                            <Typography className={classes.cardTextDate} >12</Typography>
                          </Grid>
                          <Grid item container>
                            <Typography variant="h6" className={classes.cardTextBlueEvent}>Citywide Buyer’s Retreat</Typography>
                            <Typography style={{ color: '#fff' }} variant="body1">
                              Find out how banks are responding to the changing future of interest...
                            </Typography>
                          </Grid>
                          <Grid item className={classes.eventButton}>
                            <Typography>Get More Insight</Typography>
                          </Grid>
                        </Grid>
                        <Grid container justify="flex-end" >
                          <Typography variant="h6" style={{ fontWeight: 'bold', marginTop: 12 }}>The Wagner, New York</Typography>
                        </Grid>
                      </Grid>

                    ) : null}

                    {Math.abs(activeStepEvents - 0) <= 2 ? (
                      <Grid item style={{ maxWidth: 385 }}>
                        <Grid container direction="column" className={classes.cardContainerBlueEvent} justify="space-between">
                          <Grid container direction="column" className={classes.dateContainer} justify="center" alignItems="center">
                            <Typography variant="caption" className={classes.cardTextMonth}>MAY</Typography>
                            <Typography className={classes.cardTextDate} >6</Typography>
                          </Grid>
                          <Grid item container>
                            <Typography variant="h6" className={classes.cardTextBlueEvent}>Research Exchange</Typography>
                            <Typography style={{ color: '#fff' }} variant="body1">
                              Find the best online resources to help with your investments...
                            </Typography>
                          </Grid>
                          <Grid item className={classes.eventButton}>
                            <Typography>Get More Insight</Typography>
                          </Grid>
                        </Grid>
                        <Grid container justify="flex-end" >
                          <Typography variant="h6" style={{ fontWeight: 'bold', marginTop: 12 }}>London, England</Typography>
                        </Grid>
                      </Grid>

                    ) : null}
                  </AutoPlaySwipeableViews>
                </Grid>
              </Grid>
              <Grid container justify="center" gutterBottom gutterTop >
                <img alt="f" src={logoOnly} width="50" />
              </Grid>
            </Grid>
          </Grid>

          <Grid container justify="center" alignItems="center" className={classes.navContainerMobile}>
            <Grid item xs={12}>
              <Grid container direction="row" style={{ backgroundColor: '#cccccc', paddingTop: 24 }}>

                <Grid item xs={12} container className={classes.linkPrivacyContaier} justify="space-between">
                  <Grid item xs={5} className={classes.linksBottomPrivary}>
                    <Typography>Privacy Policy</Typography>
                  </Grid>

                  <Grid item xs={5} className={classes.linksBottomPrivary}>
                    <Typography>Term of Use</Typography>
                  </Grid>
                </Grid>
                <Grid item xs={12} className={classes.siteLinks}>
                  <Typography>Site Links</Typography>
                </Grid>
                <Grid item xs={12} className={classes.links}>
                  <Typography>Home</Typography>
                </Grid>
                <Grid item xs={12} className={classes.links}>
                  <Typography>About Us</Typography>
                </Grid>
                <Grid item xs={12} className={classes.links}>
                  <Typography>Insight</Typography>
                </Grid>
                <Grid item xs={12} className={classes.links}>
                  <Typography>Events</Typography>
                </Grid>
                <Grid item xs={12} className={classes.links}>
                  <Typography>Contact Us</Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>

        </Hidden>
      </Grid>
    </>

  );
}

export default App;
