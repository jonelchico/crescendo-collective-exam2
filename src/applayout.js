import React from 'react'
import { Grid, Typography } from '@material-ui/core'

export default AppLayout() {
    return (
        <Grid>
            <Typography>App layout</Typography>
        </Grid>
    )
}